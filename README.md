# The document is now available [here](https://www.apertis.org/guides/component_guide/)

# Updating Existing Components

## Manually Updating Components from Debian

Upstream updates are usually handled automatically by the
[`ci-package-builder.yml`](https://gitlab.apertis.org/infrastructure/ci-package-builder/)
Continous Integration pipeline, which
[fetches upstream packages, merges them](https://www.apertis.org/guides/component_guide/#updating-components-from-debian)
with the Apertis contents and directly creates Merge Requests to be reviewed by
[maintainers](https://www.apertis.org/policies/contributions/#the-role-of-maintainers).

> ⚠️ Warning!
> 
> The
[automated pipeline](https://www.apertis.org/guides/component_guide/#updating-components-from-debian)
should be used to perform this task where possible.
> 
> This guide uses tools that are only provided in the package-source-builder
image. The easiest way to perform this task is to utilise the docker container.

However, in some cases it may be necessary to manually perform these
operations.

- Check out the source repository

      $ GITLAB_GROUP=pkg
      $ PACKAGE=hello
      $ git clone git@gitlab.apertis.org:${GITLAB_GROUP}/${PACKAGE}
      $ cd ${PACKAGE}

  > ✍️ Note!
  > 
  > When handling packages in your personal GitLab namespace, `GITLAB_GROUP`
  should point to that, which is your GitLab username, rather than the main
  Apertis application group `pkg`.

- Ensure we have the required `debian/` and `upstream/` branches locally

      $ CURRENT_UPSTREAM=buster
      $ git branch debian/${CURRENT_UPSTREAM} origin/debian/${CURRENT_UPSTREAM}
      $ git branch upstream/${CURRENT_UPSTREAM} origin/upstream/${CURRENT_UPSTREAM}

- Ensure we have a `pristine-lfs` branch locally:

      $ git branch pristine-lfs origin/pristine-lfs

- Pull in the latest updates Debian from using the `apertis-pkg-pull-updates` script

      $ APERTIS_RELEASE=v2021
      $ MIRROR=https://deb.debian.org/debian
      $ git checkout -b apertis/${APERTIS_RELEASE} origin/apertis/${APERTIS_RELEASE}
      $ apertis-pkg-pull-updates --package=${PACKAGE} --upstream=${CURRENT_UPSTREAM} --mirror=${MIRROR}

  This will update the `debian/...`, `upstream/...` and `pristine-lfs` branches
  as prepared previously.

- Ensure that the changes to these branches are pushed to your remote `origin`.
  It is important that these branches are pushed and in sync with the default
  remote.

      $ git push origin --follow-tags pristine-lfs debian/${CURRENT_UPSTREAM} upstream/${CURRENT_UPSTREAM}

- The upstream changes now need to be merged into the apertis branch. We will
  do this via a development branch that can be pushed for review.

      $ PROPOSED_BRANCH=wip/${GITLAB_GROUP}/${APERTIS_RELEASE}-update
      $ git checkout -b ${PROPOSED_BRANCH} apertis/${APERTIS_RELEASE}
      $ apertis-pkg-merge-updates --upstream=debian/${CURRENT_UPSTREAM} --downstream=${PROPOSED_BRANCH}

- If the merge fails, fix the conflicts and continue

      $ git add ...
      $ git merge --continue

- Create a new changelog entry for the new version

      $ DEBEMAIL="User Name <user@example.com>" dch -D apertis --local +apertis --no-auto-nmu ""

- Check `debian/changelog` and update it if needed by listing all the remaining
  Apertis changes then commit the changelog.

      $ DEBIAN_VERSION=$(dpkg-parsechangelog -S Version)
      $ git commit -s -m "Release ${PACKAGE} version ${DEBIAN_VERSION}" debian/changelog

- Push your changes for review

      $ git push origin ${PROPOSED_BRANCH}:${PROPOSED_BRANCH}

  Follow the instructions provided by the above command to create a merge request

## Manually Updating Components from a Newer Debian Release

There are circumstances, when we deviate from the default upstream. This
usually happens when:

- Packages are not available in the default distribution repository
- Packages in the default distribution repository are outdated
- Newer version of package, available in the non-default repository, is needed

For example, Apertis v2021 ships with a newer version of the Linux kernel
(5.10.x) than Debian Buster (4.9.x) on which it is based, which is the kernel
found in the next Debian release called Bullseye. In such cases, special care
needs to be taken to update packages from their respective upstreams.

> ⚠️ Warning!
> 
> The
[automated pipeline](https://www.apertis.org/guides/component_guide/#updating-components-from-debian)
should be used to perform this task where possible.
> 
> This guide uses tools that are only provided in the package-source-builder
image. The easiest way to perform this task is to utilise the docker container.

Below are a set of steps which can be adapted to such exception packages. Let
us assume that for such repository, the package was picked from Debian Bullseye
instead of Buster

- Clone the repository:

      $ GITLAB_GROUP=pkg
      $ PACKAGE=hello
      $ git clone git@gitlab.apertis.org:${GITLAB_GROUP}/${PACKAGE}
      $ cd ${PACKAGE}

- If the required `debian/` and `upstream/` branches don't yet exist in your
  repository, create them based on the version that you have for instance. For
  instance, if you currently have a version of Apertis based on Debian Buster
  and wish to use the updated package in Bullseye:

      $ CURRENT_UPSTREAM=buster
      $ DESIRED_UPSTREAM=bullseye
      $ git push origin origin/upstream/${CURRENT_UPSTREAM}:refs/heads/upstream/${DESIRED_UPSTREAM}
      $ git push origin origin/debian/${CURRENT_UPSTREAM}:refs/heads/debian/${DESIRED_UPSTREAM}

- Ensure you have the required branches locally:

      $ DESIRED_UPSTREAM=bullseye
      $ git branch debian/${DESIRED_UPSTREAM} origin/debian/${DESIRED_UPSTREAM}
      $ git branch upstream/${DESIRED_UPSTREAM} origin/upstream/${DESIRED_UPSTREAM}

- Ensure we have a `pristine-lfs` branch locally:

      $ git branch pristine-lfs origin/pristine-lfs

- Pull in new updates using the `apertis-pkg-pull-updates` script, passing in
  the deviated upstream:

      $ APERTIS_RELEASE=v2021
      $ MIRROR=https://deb.debian.org/debian
      $ git checkout -b apertis/${APERTIS_RELEASE} origin/apertis/${APERTIS_RELEASE}
      $ apertis-pkg-pull-updates --package ${PACKAGE} --upstream ${DESIRED_UPSTREAM} --mirror ${MIRROR}

  This will update the `debian/...` and `upstream/...` branch as prepared
  previously.

- Ensure that the changes to these branches are pushed to your remote `origin`.
  It is important that these branches are pushed and in sync with the default
  remote.

      $ git push origin --follow-tags pristine-lfs debian/${DESIRED_UPSTREAM} upstream/${DESIRED_UPSTREAM}

- The upstream changes now need to be merged into the apertis branch. We will
  do this via a development branch that can be pushed for review.

      $ PROPOSED_BRANCH=wip/${GITLAB_GROUP}/${APERTIS_RELEASE}-update-from-${DESIRED_UPSTREAM}
      $ git checkout -b ${PROPOSED_BRANCH} apertis/${APERTIS_RELEASE}
      $ apertis-pkg-merge-updates --upstream=debian/${DESIRED_UPSTREAM} --downstream=${PROPOSED_BRANCH}

- If the merge fails, fix the conflicts and continue

      $ git add ...
      $ git merge --continue

- Create a new changelog entry for the new version

      $ DEBEMAIL="User Name <user@example.com>" dch -D apertis --local +apertis --no-auto-nmu ""

- Check `debian/changelog` and update it if needed by listing all the remaining
  Apertis changes then commit the changelog.

      $ DEBIAN_VERSION=$(dpkg-parsechangelog -S Version)
      $ git commit -s -m "Release ${PACKAGE} version ${DEBIAN_VERSION}" debian/changelog

- Push your changes for review

      $ git push origin ${PROPOSED_BRANCH}:${PROPOSED_BRANCH}

  Follow the instructions provided by the above command to create a merge request
